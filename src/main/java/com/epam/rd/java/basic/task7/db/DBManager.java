package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {
    private static final String CONNECTION_URL ="jdbc:derby:memory:testdb;create=true"; //"jdbc:mysql://localhost:3306/test2db?user=root&password=qwerty";
    private final static String INSERT_USER_NEW = "INSERT into users(login) values(?)";
    private final static String SELECT_USER = "SELECT * FROM users WHERE login=?";
    private final static String SELECT_TEAM = "SELECT * FROM teams WHERE name=?";
    private final static String INSERT_TEAM_NEW = "INSERT into teams(name) values(?)";
    private final static String INSERT_TEAMS_FOR_USER_NEW = "INSERT into users_teams(user_id, team_id)" +
            " values(?,?)";
    public static final String SELECT_ALL_TEAMS = "SELECT * FROM teams";
    public static final String SELECT_USER_TEAMS = "SELECT * FROM users_teams WHERE user_id=?";
    public static final String SELECT_ALL_USERS = "SELECT * FROM users ORDER BY id";
    public static final String SELECT_TEAMS_WHERE_ID = "SELECT * FROM teams WHERE id=?";
    public static final String UPDATE_TEAMS_SET_NAME_WHERE_ID = "UPDATE teams  SET name=? WHERE id=?";
    public static final String DELETE_FROM_USERS_WHERE_LOGIN = "DELETE FROM users WHERE login=?";
    public static final String DELETE_FROM_TEAMS_WHERE_NAME = "DELETE FROM teams WHERE name=?";
    private static DBManager instance;


    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        List<User> userList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(CONNECTION_URL);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SELECT_ALL_USERS)) {
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setLogin(resultSet.getString("login"));
                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Error in findAllUsers", e);
        }
        return userList;
    }

    public boolean insertUser(User user) throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DriverManager.getConnection(CONNECTION_URL);
            preparedStatement = connection.prepareStatement(INSERT_USER_NEW, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                user.setId(rs.getInt(1));
            }
        } catch (SQLException e) {
            System.out.println("ex");
            e.printStackTrace();
            throw new DBException("Error in insertUser", e);
        } finally {
            close(connection);
            close(preparedStatement);
        }
        return true;
    }

    private void close(AutoCloseable connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        try (Connection conn = DriverManager.getConnection(CONNECTION_URL);
             PreparedStatement stmt = conn.prepareStatement(DELETE_FROM_USERS_WHERE_LOGIN)) {
            for (User user : users) {
                stmt.setString(1, user.getLogin());
                stmt.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Error in deleteTeam", e);
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        User user = new User();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DriverManager.getConnection(CONNECTION_URL);
            preparedStatement = connection.prepareStatement(SELECT_USER);
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user.setId(resultSet.getInt("id"));
                user.setLogin(resultSet.getString("login"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Error in getUser", e);
        } finally {
            close(connection);
            close(preparedStatement);
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = new Team();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DriverManager.getConnection(CONNECTION_URL);
            preparedStatement = connection.prepareStatement(SELECT_TEAM);
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                team.setId(resultSet.getInt("id"));
                team.setName(resultSet.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Error in getTeam", e);
        } finally {
            close(connection);
            close(preparedStatement);
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teamList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(CONNECTION_URL);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SELECT_ALL_TEAMS)) {
            while (resultSet.next()) {
                Team team = new Team();
                team.setId(resultSet.getInt("id"));
                team.setName(resultSet.getString("name"));
                teamList.add(team);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Error in findAllUsers", e);
        }
        return teamList;
    }

    public boolean insertTeam(Team team) throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DriverManager.getConnection(CONNECTION_URL);
            preparedStatement = connection.prepareStatement(INSERT_TEAM_NEW, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, team.getName());
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                team.setId(rs.getInt(1));
            }
        } catch (SQLException e) {
            System.out.println("ex");
            e.printStackTrace();
            throw new DBException("Error in insertTeam", e);
        } finally {
            close(connection);
            close(preparedStatement);
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DriverManager.getConnection(CONNECTION_URL);
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(INSERT_TEAMS_FOR_USER_NEW);
            for (Team team : teams) {
                preparedStatement.setInt(1, user.getId());
                preparedStatement.setInt(2, team.getId());
                preparedStatement.execute();
            }
            connection.commit();
        } catch (SQLException e) {
            System.out.println("ex");
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
            throw new DBException("Error in setTeamsForUser", e);
        } finally {
            close(connection);
            close(preparedStatement);
        }
        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        List<Team> teamList = new ArrayList<>();
        try {
            connection = DriverManager.getConnection(CONNECTION_URL);
            preparedStatement = connection.prepareStatement(SELECT_USER_TEAMS);
            preparedStatement.setInt(1, user.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int temp = resultSet.getInt("team_id");
                preparedStatement = connection.prepareStatement(SELECT_TEAMS_WHERE_ID);
                preparedStatement.setInt(1, temp);
                ResultSet resultSet2 = preparedStatement.executeQuery();
                while (resultSet2.next()) {
                    Team team = new Team();
                    team.setId(resultSet2.getInt("id"));
                    team.setName(resultSet2.getString("name"));
                    teamList.add(team);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Error in getUserTeams", e);
        } finally {
            close(connection);
            close(preparedStatement);
        }
        return teamList;
    }


    public boolean deleteTeam(Team team) throws DBException {
        try (Connection conn = DriverManager.getConnection(CONNECTION_URL);
             PreparedStatement stmt = conn.prepareStatement(DELETE_FROM_TEAMS_WHERE_NAME)) {

            stmt.setString(1, team.getName());
            stmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Error in deleteTeam", e);
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        try (Connection conn = DriverManager.getConnection(CONNECTION_URL);
             PreparedStatement stmt = conn.prepareStatement(UPDATE_TEAMS_SET_NAME_WHERE_ID)) {

            stmt.setString(1, team.getName());
            stmt.setInt(2, team.getId());

            stmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Error in updateTeam", e);
        }
        return true;
    }
}
